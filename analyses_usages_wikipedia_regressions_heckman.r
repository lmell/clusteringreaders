# Environment -----------------------------------------------------------
# Careful, this line is very computer and operating system dependent
# setwd("C:/Users/Administrateur/Desktop/Recherche Enseignement/R/25_wikipedia_cluster_readers")
# Better use these lines that tell the computer to use the file's path and folder
library(this.path)

local_path <- gsub('analyses_usages_wikipedia_regressions_heckman.r', '', this.path())
setwd(local_path)
getwd()
source("fonctions_analyses.R")
source("visualizationHelpers.R")

library(ggrepel)
library(magrittr)
library(ggdendro)
library(stargazer)

bdd <- read_delim("data_filtered.csv", delim = ";", escape_double = FALSE, trim_ws = TRUE)

variables <- read_excel("interets.xlsx", sheet = "interets")

# Applying more explicit naming conventions ---------------------------------

# Fixing names to match the dictionary
variables$`NEW CODE` %<>% gsub("[", "", ., fixed = T)
variables$`NEW CODE` %<>% gsub("]", "", ., fixed = T)
variables$`NEW CODE` %<>% gsub("_", "", ., fixed = T)
variables$`NEW CODE`[variables$`NEW CODE` == "newcountrybis"] <- "new_country"
variables$`NEW CODE`[variables$`NEW CODE` == "newCSP1"] <- "new_CSP_1"

bdd <- bdd[, variables$`NEW CODE`]
names(bdd) <- variables$code_perso

# recoding of active variables ---------------------------------
## if required, the irec() function (questionr package) opens an interface for easy variable recoding





# recoding of illustratives variables ---------------------------------------


# Gender
bdd$Gender[bdd$Gender == "a man"] <- "Man"
bdd$Gender[bdd$Gender == "a woman"] <- "Woman"
bdd$Gender[bdd$Gender == "other, No binary"] <- "Other"
bdd$Gender[bdd$Gender == "you do not want to answer"] <- "Other"
bdd$Gender <- factor(bdd$Gender, levels = c("Other", "Man", "Woman"))


# Age
bdd$Age <- factor(bdd$Age, levels = c("Under 18", "Between 18 and 34", "Between 35 and 44", "Between 45 and 54", "Between 55 and 64", "Over 64", "DK-NA-RA"))


# Matrimonial
bdd$Matrimonial <- factor(bdd$Matrimonial, levels = c("Single", "In couple, but you do not live under the same roof", "In couple, and you do live under the same roof", "DK-NA-RA"))


# Education
## this variable could be recoded
bdd$Diploma[bdd$Diploma == "No diploma"] <- "No diploma"
bdd$Diploma[bdd$Diploma == "Primary education"] <- "Primary education"
bdd$Diploma[bdd$Diploma == "Secondary education cycle 1 (end of Middle School)"] <- "Secondary education cycle 1"
bdd$Diploma[bdd$Diploma == "Secondary education cycle 2 (A Level, end of High school)"] <- "Secondary education cycle 2"
bdd$Diploma[bdd$Diploma == "Higher education (2 years or less of college education)"] <- "Higher education"
bdd$Diploma[bdd$Diploma == "Bachelor's degree or equivalent (3 years of College education)"] <- "Bachelor's degree or equivalent"
bdd$Diploma[bdd$Diploma == "Master's degree or equivalent"] <- "Master's degree or equivalent"
bdd$Diploma[bdd$Diploma == "Doctorate or equivalent"] <- "Doctorate or equivalent"
bdd$Diploma[bdd$Diploma == "DK-NA-RA"] <- "DK-NA-RA"
bdd$Diploma <- factor(bdd$Diploma, levels = c("No diploma", "Primary education", "Secondary education cycle 1", "Secondary education cycle 2", "Higher education", 
                                              "Bachelor's degree or equivalent", "Master's degree or equivalent", "Doctorate or equivalent", "DK-NA-RA"))


# Professional situation
## this variable could be recoded
bdd$ProfessionalSituation[bdd$ProfessionalSituation == "You are in full-time employment"] <- "Full-time employment"
bdd$ProfessionalSituation[bdd$ProfessionalSituation == "You are in part-time employment"] <- "Part-time employment"
bdd$ProfessionalSituation[bdd$ProfessionalSituation == "You are looking for a job"] <- "Looking for a job"
bdd$ProfessionalSituation[bdd$ProfessionalSituation == "You are retired"] <- "Retired"
bdd$ProfessionalSituation[bdd$ProfessionalSituation == "You are in middle or high school"] <- "Middle or high school"
bdd$ProfessionalSituation[bdd$ProfessionalSituation == "You are in college, in post high school studies"] <- "College"
bdd$ProfessionalSituation[bdd$ProfessionalSituation == "You are studying but have already worked (or are working)"] <- "Studying but have already worked"
bdd$ProfessionalSituation[bdd$ProfessionalSituation == "You are temporarily out of work (parental leave, temporarily at home)"] <- "Temporarily out of work"
bdd$ProfessionalSituation[bdd$ProfessionalSituation == "You are not \"\"working\"\" (disability, at home)"] <- "Not working"
bdd$ProfessionalSituation[bdd$ProfessionalSituation == "DK-NA-RA"] <- "DK-NA-RA"
bdd$ProfessionalSituation <- factor(bdd$ProfessionalSituation, levels = c("Full-time employment", "Part-time employment", "Looking for a job", "Retired", "Middle or high school", 
                                                                          "College", "Studying but have already worked", "Temporarily out of work", "Not working", "DK-NA-RA"))


# CSP
bdd$CSP[bdd$CSP == "Clerical Support Workers (secretary, receptionist, administrative jobs)"] <- "Clerical Support Workers"
bdd$CSP[bdd$CSP == "Craft and Related Trades Workers"] <- "Craft Workers"
bdd$CSP[bdd$CSP == "Direct service personnel to individuals, shopkeeper and sales workers"] <- "Direct service personnel"
bdd$CSP[bdd$CSP == "Director, executive and manager"] <- "Director, executive and manager"
bdd$CSP[bdd$CSP == "Intellectual and scientific profession"] <- "Intellectual and scientific profession"
bdd$CSP[bdd$CSP == "Intermediate profession (technician, nurse, etc.)"] <- "Intermediate profession"
bdd$CSP[bdd$CSP == "Military jobs"] <- "Military jobs"
bdd$CSP[bdd$CSP == "Never worked"] <- "Never worked"
bdd$CSP[bdd$CSP == "Plant and machine operator, and assembly worker"] <- "Factory worker"
bdd$CSP[bdd$CSP == "Profession of assistance, No qualified workers or employees (jenitors, kitchen aids, care jobs...)"] <- "No qualified workers"
bdd$CSP[bdd$CSP == "Skilled Agricultural, Forestry and Fishery Workers"] <- "Agriculture, forestry and fishing"
bdd$CSP <- bdd$CSP %>%  fct_explicit_na("DK-NA-RA")
bdd$CSP <- factor(bdd$CSP, levels = c("Director, executive and manager", "Intellectual and scientific profession", "Intermediate profession", "Clerical Support Workers", 
                                      "Direct service personnel", "Agriculture, forestry and fishing", "Craft Workers", "Factory worker", "No qualified workers", 
                                      "Military jobs", "Never worked", "DK-NA-RA"))


# Financial Ressources Level
bdd$FinancialResourceLevel[bdd$FinancialResourceLevel == "You find life very difficult financially speaking"] <- "Life very difficult"
bdd$FinancialResourceLevel[bdd$FinancialResourceLevel == "You find life difficult"] <- "Life difficult"
bdd$FinancialResourceLevel[bdd$FinancialResourceLevel == "You are getting by"] <- "You are getting by"
bdd$FinancialResourceLevel[bdd$FinancialResourceLevel == "You have a comfortable life"] <- "Comfortable life"
bdd$FinancialResourceLevel[bdd$FinancialResourceLevel == "You have a very comfortable life, financially speaking"] <- "Very comfortable life"
bdd$FinancialResourceLevel[bdd$FinancialResourceLevel == "DK-NA-RA"] <- "DK-NA-RA"
bdd$FinancialResourceLevel <- factor(bdd$FinancialResourceLevel, levels = c("Life very difficult", "Life difficult", "You are getting by", "Comfortable life", "Very comfortable life", "DK-NA-RA"))


# Time Availability
bdd$TimeAvailability <- factor(bdd$TimeAvailability, levels = c("No free time at all", "A little free time", "Some free time", "A lot of free time", "DK-NA-RA"))


# Cultural Activities
bdd$CulturalActivitiesParticipation <- factor(bdd$CulturalActivitiesParticipation, levels = c("Never", "At least once a year", "At least once a month", "At least once a week", "At least once a day", "Several times a day", "DK-NA-RA"))
bdd$SportsActivitiesParticipation <- factor(bdd$SportsActivitiesParticipation, levels = c("Never", "At least once a year", "At least once a month", "At least once a week", "At least once a day", "Several times a day", "DK-NA-RA"))
bdd$ArtisticActivitiesParticipation <- factor(bdd$ArtisticActivitiesParticipation, levels = c("Never", "At least once a year", "At least once a month", "At least once a week", "At least once a day", "Several times a day", "DK-NA-RA"))
bdd$SocialActivitiesParticipation <- factor(bdd$SocialActivitiesParticipation, levels = c("Never", "At least once a year", "At least once a month", "At least once a week", "At least once a day", "Several times a day", "DK-NA-RA"))


# Digital Activities
bdd$DigitalActivitiesListenRadio <- factor(bdd$DigitalActivitiesListenRadio, levels = c("Not at all", "Less than 1 hour", "From 1 to 2 hours", "From 2 to 3 hours", "From 3 to 4 hours", "4 hours or more", "DK-NA-RA"))
bdd$DigitalActivitiesWatchTelevision <- factor(bdd$DigitalActivitiesWatchTelevision, levels = c("Not at all", "Less than 1 hour", "From 1 to 2 hours", "From 2 to 3 hours", "From 3 to 4 hours", "4 hours or more", "DK-NA-RA"))
bdd$DigitalActivitiesWatchVideo <- factor(bdd$DigitalActivitiesWatchVideo, levels = c("Not at all", "Less than 1 hour", "From 1 to 2 hours", "From 2 to 3 hours", "From 3 to 4 hours", "4 hours or more", "DK-NA-RA"))
bdd$DigitalActivitiesListenMusic <- factor(bdd$DigitalActivitiesListenMusic, levels = c("Not at all", "Less than 1 hour", "From 1 to 2 hours", "From 2 to 3 hours", "From 3 to 4 hours", "4 hours or more", "DK-NA-RA"))
bdd$DigitalActivitiesPlayConsol <- factor(bdd$DigitalActivitiesPlayConsol, levels = c("Not at all", "Less than 1 hour", "From 1 to 2 hours", "From 2 to 3 hours", "From 3 to 4 hours", "4 hours or more", "DK-NA-RA"))
bdd$DigitalActivitiesSocialNetworkSites <- factor(bdd$DigitalActivitiesSocialNetworkSites, levels = c("Not at all", "Less than 1 hour", "From 1 to 2 hours", "From 2 to 3 hours", "From 3 to 4 hours", "4 hours or more", "DK-NA-RA"))
bdd$DigitalActivitiesInternetEntertainment <- factor(bdd$DigitalActivitiesInternetEntertainment, levels = c("Not at all", "Less than 1 hour", "From 1 to 2 hours", "From 2 to 3 hours", "From 3 to 4 hours", "4 hours or more", "DK-NA-RA"))
bdd$DigitalActivitiesInternetToWork <- factor(bdd$DigitalActivitiesInternetToWork, levels = c("Not at all", "Less than 1 hour", "From 1 to 2 hours", "From 2 to 3 hours", "From 3 to 4 hours", "4 hours or more", "DK-NA-RA"))
bdd$DigitalActivitiesStopInternet <- factor(bdd$DigitalActivitiesStopInternet, levels = c("Before 9pm", "Between 9 and 10pm", "Between 10 and 11pm", "Between 11 and 12pm", "After midnight", "DK-NA-RA"))


# Paper Readings
bdd$PaperReadingsBooks <- factor(bdd$PaperReadingsBooks, levels = c("Never", "At least once a year", "At least once a month", "At least once a week", "At least once a day", "Several times a day", "DK-NA-RA"))
bdd$PaperReadingsComics <- factor(bdd$PaperReadingsComics, levels = c("Never", "At least once a year", "At least once a month", "At least once a week", "At least once a day", "Several times a day", "DK-NA-RA"))
bdd$PaperReadingsNews <- factor(bdd$PaperReadingsNews, levels = c("Never", "At least once a year", "At least once a month", "At least once a week", "At least once a day", "Several times a day", "DK-NA-RA"))
bdd$PaperReadingsFashion <- factor(bdd$PaperReadingsFashion, levels = c("Never", "At least once a year", "At least once a month", "At least once a week", "At least once a day", "Several times a day", "DK-NA-RA"))
bdd$PaperReadingsSport <- factor(bdd$PaperReadingsSport, levels = c("Never", "At least once a year", "At least once a month", "At least once a week", "At least once a day", "Several times a day", "DK-NA-RA"))
bdd$PaperReadingsMusic <- factor(bdd$PaperReadingsMusic, levels = c("Never", "At least once a year", "At least once a month", "At least once a week", "At least once a day", "Several times a day", "DK-NA-RA"))
bdd$PaperReadingsMovie <- factor(bdd$PaperReadingsMovie, levels = c("Never", "At least once a year", "At least once a month", "At least once a week", "At least once a day", "Several times a day", "DK-NA-RA"))
bdd$PaperReadingsNature <- factor(bdd$PaperReadingsNature, levels = c("Never", "At least once a year", "At least once a month", "At least once a week", "At least once a day", "Several times a day", "DK-NA-RA"))
bdd$PaperReadingsSciences <- factor(bdd$PaperReadingsSciences, levels = c("Never", "At least once a year", "At least once a month", "At least once a week", "At least once a day", "Several times a day", "DK-NA-RA"))
bdd$PaperReadingsHealth <- factor(bdd$PaperReadingsHealth, levels = c("Never", "At least once a year", "At least once a month", "At least once a week", "At least once a day", "Several times a day", "DK-NA-RA"))
bdd$PaperReadingsVideoGames <- factor(bdd$PaperReadingsVideoGames, levels = c("Never", "At least once a year", "At least once a month", "At least once a week", "At least once a day", "Several times a day", "DK-NA-RA"))


# Digital Readings
bdd$DigitalReadingsBooks <- factor(bdd$DigitalReadingsBooks, levels = c("Never", "At least once a year", "At least once a month", "At least once a week", "At least once a day", "Several times a day", "DK-NA-RA"))
bdd$DigitalReadingsComics <- factor(bdd$DigitalReadingsComics, levels = c("Never", "At least once a year", "At least once a month", "At least once a week", "At least once a day", "Several times a day", "DK-NA-RA"))
bdd$DigitalReadingsNews <- factor(bdd$DigitalReadingsNews, levels = c("Never", "At least once a year", "At least once a month", "At least once a week", "At least once a day", "Several times a day", "DK-NA-RA"))
bdd$DigitalReadingsFashion <- factor(bdd$DigitalReadingsFashion, levels = c("Never", "At least once a year", "At least once a month", "At least once a week", "At least once a day", "Several times a day", "DK-NA-RA"))
bdd$DigitalReadingsSport <- factor(bdd$DigitalReadingsSport, levels = c("Never", "At least once a year", "At least once a month", "At least once a week", "At least once a day", "Several times a day", "DK-NA-RA"))
bdd$DigitalReadingsMusic <- factor(bdd$DigitalReadingsMusic, levels = c("Never", "At least once a year", "At least once a month", "At least once a week", "At least once a day", "Several times a day", "DK-NA-RA"))
bdd$DigitalReadingsMovie <- factor(bdd$DigitalReadingsMovie, levels = c("Never", "At least once a year", "At least once a month", "At least once a week", "At least once a day", "Several times a day", "DK-NA-RA"))
bdd$DigitalReadingsNature <- factor(bdd$DigitalReadingsNature, levels = c("Never", "At least once a year", "At least once a month", "At least once a week", "At least once a day", "Several times a day", "DK-NA-RA"))
bdd$DigitalReadingsSciences <- factor(bdd$DigitalReadingsSciences, levels = c("Never", "At least once a year", "At least once a month", "At least once a week", "At least once a day", "Several times a day", "DK-NA-RA"))
bdd$DigitalReadingsHealth <- factor(bdd$DigitalReadingsHealth, levels = c("Never", "At least once a year", "At least once a month", "At least once a week", "At least once a day", "Several times a day", "DK-NA-RA"))
bdd$DigitalReadingsVideoGames <- factor(bdd$DigitalReadingsVideoGames, levels = c("Never", "At least once a year", "At least once a month", "At least once a week", "At least once a day", "Several times a day", "DK-NA-RA"))


# Get Information
bdd$GetInInformationTelevision <- factor(bdd$GetInInformationTelevision, levels = c("Never", "At least once a year", "At least once a month", "At least once a week", "At least once a day", "Several times a day", "DK-NA-RA"))
bdd$GetInInformationRadio <- factor(bdd$GetInInformationRadio, levels = c("Never", "At least once a year", "At least once a month", "At least once a week", "At least once a day", "Several times a day", "DK-NA-RA"))
bdd$GetInInformationNewspaper <- factor(bdd$GetInInformationNewspaper, levels = c("Never", "At least once a year", "At least once a month", "At least once a week", "At least once a day", "Several times a day", "DK-NA-RA"))
bdd$GetInInformationScientificPublication <- factor(bdd$GetInInformationScientificPublication, levels = c("Never", "At least once a year", "At least once a month", "At least once a week", "At least once a day", "Several times a day", "DK-NA-RA"))
bdd$GetInInformationSearchEngine <- factor(bdd$GetInInformationSearchEngine, levels = c("Never", "At least once a year", "At least once a month", "At least once a week", "At least once a day", "Several times a day", "DK-NA-RA"))
bdd$GetInInformationSocialNetworkSitesInformation <- factor(bdd$GetInInformationSocialNetworkSitesInformation, levels = c("Never", "At least once a year", "At least once a month", "At least once a week", "At least once a day", "Several times a day", "DK-NA-RA"))


# Information Trust
bdd$InformationTrustTelevision[bdd$InformationTrustTelevision == "Not at all reliable"] <- "Unreliable"
bdd$InformationTrustTelevision[bdd$InformationTrustTelevision == "Somewhat unreliable"] <- "Unreliable"
bdd$InformationTrustTelevision[bdd$InformationTrustTelevision == "Somewhat reliable"] <- "Reliable"
bdd$InformationTrustTelevision[bdd$InformationTrustTelevision == "Very reliable"] <- "Reliable"
bdd$InformationTrustTelevision <- factor(bdd$InformationTrustTelevision, levels = c("Reliable", "Unreliable", "DK-NA-RA"))

bdd$InformationTrustRadio[bdd$InformationTrustRadio == "Not at all reliable"] <- "Unreliable"
bdd$InformationTrustRadio[bdd$InformationTrustRadio == "Somewhat unreliable"] <- "Unreliable"
bdd$InformationTrustRadio[bdd$InformationTrustRadio == "Somewhat reliable"] <- "Reliable"
bdd$InformationTrustRadio[bdd$InformationTrustRadio == "Very reliable"] <- "Reliable"
bdd$InformationTrustRadio <- factor(bdd$InformationTrustRadio, levels = c("Reliable", "Unreliable", "DK-NA-RA"))

bdd$InformationTrustNewspaper[bdd$InformationTrustNewspaper == "Not at all reliable"] <- "Unreliable"
bdd$InformationTrustNewspaper[bdd$InformationTrustNewspaper == "Somewhat unreliable"] <- "Unreliable"
bdd$InformationTrustNewspaper[bdd$InformationTrustNewspaper == "Somewhat reliable"] <- "Reliable"
bdd$InformationTrustNewspaper[bdd$InformationTrustNewspaper == "Very reliable"] <- "Reliable"
bdd$InformationTrustNewspaper <- factor(bdd$InformationTrustNewspaper, levels = c("Reliable", "Unreliable", "DK-NA-RA"))

bdd$InformationTrustScientificPublication[bdd$InformationTrustScientificPublication == "Not at all reliable"] <- "Unreliable"
bdd$InformationTrustScientificPublication[bdd$InformationTrustScientificPublication == "Somewhat unreliable"] <- "Unreliable"
bdd$InformationTrustScientificPublication[bdd$InformationTrustScientificPublication == "Somewhat reliable"] <- "Reliable"
bdd$InformationTrustScientificPublication[bdd$InformationTrustScientificPublication == "Very reliable"] <- "Reliable"
bdd$InformationTrustScientificPublication <- factor(bdd$InformationTrustScientificPublication, levels = c("Reliable", "Unreliable", "DK-NA-RA"))

bdd$InformationTrustSearchEngine[bdd$InformationTrustSearchEngine == "Not at all reliable"] <- "Unreliable"
bdd$InformationTrustSearchEngine[bdd$InformationTrustSearchEngine == "Somewhat unreliable"] <- "Unreliable"
bdd$InformationTrustSearchEngine[bdd$InformationTrustSearchEngine == "Somewhat reliable"] <- "Reliable"
bdd$InformationTrustSearchEngine[bdd$InformationTrustSearchEngine == "Very reliable"] <- "Reliable"
bdd$InformationTrustSearchEngine <- factor(bdd$InformationTrustSearchEngine, levels = c("Reliable", "Unreliable", "DK-NA-RA"))

bdd$InformationTrustSocialNetworkSitesInformation[bdd$InformationTrustSocialNetworkSitesInformation == "Not at all reliable"] <- "Unreliable"
bdd$InformationTrustSocialNetworkSitesInformation[bdd$InformationTrustSocialNetworkSitesInformation == "Somewhat unreliable"] <- "Unreliable"
bdd$InformationTrustSocialNetworkSitesInformation[bdd$InformationTrustSocialNetworkSitesInformation == "Somewhat reliable"] <- "Reliable"
bdd$InformationTrustSocialNetworkSitesInformation[bdd$InformationTrustSocialNetworkSitesInformation == "Very reliable"] <- "Reliable"
bdd$InformationTrustSocialNetworkSitesInformation <- factor(bdd$InformationTrustSocialNetworkSitesInformation, levels = c("Reliable", "Unreliable", "DK-NA-RA"))

# Digital Skills
bdd$DigitalSkillsResearch <- factor(bdd$DigitalSkillsResearch, levels = c("Not at all comfortable", "Not very comfortable", "Comfortable", "Very comfortable", "No applicable"))
bdd$DigitalSkillsBrowse <- factor(bdd$DigitalSkillsBrowse, levels = c("Not at all comfortable", "Not very comfortable", "Comfortable", "Very comfortable", "No applicable"))
bdd$DigitalSkillsDownload <- factor(bdd$DigitalSkillsDownload, levels = c("Not at all comfortable", "Not very comfortable", "Comfortable", "Very comfortable", "No applicable"))
bdd$DigitalSkillsManagePasswords <- factor(bdd$DigitalSkillsManagePasswords, levels = c("Not at all comfortable", "Not very comfortable", "Comfortable", "Very comfortable", "No applicable"))
bdd$DigitalSkillsBuyOnline <- factor(bdd$DigitalSkillsBuyOnline, levels = c("Not at all comfortable", "Not very comfortable", "Comfortable", "Very comfortable", "No applicable"))
bdd$DigitalSkillsInstallApplicationSmartphone <- factor(bdd$DigitalSkillsInstallApplicationSmartphone, levels = c("Not at all comfortable", "Not very comfortable", "Comfortable", "Very comfortable", "No applicable"))
bdd$DigitalSkillsInstallSoftwareComputer <- factor(bdd$DigitalSkillsInstallSoftwareComputer, levels = c("Not at all comfortable", "Not very comfortable", "Comfortable", "Very comfortable", "No applicable"))
bdd$DigitalSkillsReadWriteMail <- factor(bdd$DigitalSkillsReadWriteMail, levels = c("Not at all comfortable", "Not very comfortable", "Comfortable", "Very comfortable", "No applicable"))
bdd$DigitalSkillsWriteComplexDocument <- factor(bdd$DigitalSkillsWriteComplexDocument, levels = c("Not at all comfortable", "Not very comfortable", "Comfortable", "Very comfortable", "No applicable"))


## When people have strated using wikipedia
# bdd$DateStartUseWikipediaLikeInternet <- factor(bdd$DateStartUseWikipediaLikeInternet)
# levels(bdd$DateStartUseWikipediaLikeInternet) <- c("DK-NA-RA-Not applicable","No", "DK-NA-RA-Not applicable","Yes")
# bdd$DateStartUseWikipediaBefore2005 <- factor(bdd$DateStartUseWikipediaBefore2005)
# levels(bdd$DateStartUseWikipediaBefore2005) <- c("DK-NA-RA-Not applicable","No", "DK-NA-RA-Not applicable","Yes")


# time spent on reading wikipedia, big readers are regrouped
# bdd$TimeSpentReading <- factor(bdd$TimeSpentReading)
# levels(bdd$TimeSpentReading) <- c("1 hour to less than 5 hours per week","10 hours per week or more","10 hours per week or more","5 hours to less than 10 hours per week","DK-NA-RA","Less than 1 hour per month","Less than 1 hour per week")
# table(bdd$TimeSpentReading)
# variables reordered to be easier to read
# bdd$TimeSpentReading  <- factor(bdd$TimeSpentReading, levels = c("DK-NA-RA","Less than 1 hour per month","Less than 1 hour per week","1 hour to less than 5 hours per week","5 hours to less than 10 hours per week","10 hours per week or more"))

# For the uses of Wikipedia as a resource, we systematically group together other online and offline resources and the RR
## professional activities
### those who never do this will be recoded as "No".

bdd$UsesProSettleDebate <- factor(bdd$UsesProSettleDebate)
bdd$UsesProSettleDebateWikipedia <- factor(bdd$UsesProSettleDebateWikipedia)
levels(bdd$UsesProSettleDebateWikipedia) <- c("No","No","No","Yes, but rarely","Yes, often","Yes, sometimes")
bdd$UsesProSettleDebateWikipedia <- factor(bdd$UsesProSettleDebateWikipedia, levels =  c("No","Yes, but rarely","Yes, sometimes","Yes, often"))
table(bdd$UsesProSettleDebateWikipedia)


table(bdd$UsesProCheckSomethingWikipedia)
bdd$UsesProCheckSomethingWikipedia <- factor(bdd$UsesProCheckSomethingWikipedia)
bdd$UsesProCheckSomethingWikipedia %<>% recode_factor(
  "DK-NA-RA" = "No",
  "No, you prefer other offline resource(s) for that" = "No",
  "No, you prefer other online resource(s) for that" = "No"
)
bdd$UsesProCheckSomethingWikipedia %<>% as.factor() %>% relevel(ref = "No") 

table(bdd$UsesProTrainWikipedia)
bdd$UsesProTrainWikipedia <- factor(bdd$UsesProTrainWikipedia)
bdd$UsesProTrainWikipedia %<>% recode_factor(
  "DK-NA-RA" = "No",
  "No, you prefer other offline resource(s) for that" = "No",
  "No, you prefer other online resource(s) for that" = "No"
)
bdd$UsesProTrainWikipedia %<>% as.factor() %>% relevel(ref = "No") 

table(bdd$UsesProWatchCuriosityWikipedia)
bdd$UsesProWatchCuriosityWikipedia <- factor(bdd$UsesProWatchCuriosityWikipedia)
bdd$UsesProWatchCuriosityWikipedia %<>% recode_factor(
  "DK-NA-RA" = "No",
  "No, you prefer other offline resource(s) for that" = "No",
  "No, you prefer other online resource(s) for that" = "No"
)
bdd$UsesProWatchCuriosityWikipedia %<>% as.factor() %>% relevel(ref = "No") 


## private activities


table(bdd$UsesPersoSettleDebateWikipedia)
bdd$UsesPersoSettleDebateWikipedia <- factor(bdd$UsesPersoSettleDebateWikipedia)
bdd$UsesPersoSettleDebateWikipedia %<>% recode_factor(
  "DK-NA-RA" = "No",
  "No, you prefer other offline resource(s) for that" = "No",
  "No, you prefer other online resource(s) for that" = "No"
)
bdd$UsesPersoSettleDebateWikipedia %<>% as.factor() %>% relevel(ref = "No") 


table(bdd$UsesPersoTrainWikipedia)
bdd$UsesPersoTrainWikipedia <- factor(bdd$UsesPersoTrainWikipedia)
bdd$UsesPersoTrainWikipedia %<>% recode_factor(
  "DK-NA-RA" = "No",
  "No, you prefer other offline resource(s) for that" = "No",
  "No, you prefer other online resource(s) for that" = "No"
)
bdd$UsesPersoTrainWikipedia %<>% as.factor() %>% relevel(ref = "No") 



table(bdd$UsesPersoWatchCuriosityWikipedia)
bdd$UsesPersoWatchCuriosityWikipedia  <- factor(bdd$UsesPersoWatchCuriosityWikipedia )
bdd$UsesPersoWatchCuriosityWikipedia  %<>% recode_factor(
  "DK-NA-RA" = "No",
  "No, you prefer other offline resource(s) for that" = "No",
  "No, you prefer other online resource(s) for that" = "No"
)
bdd$UsesPersoWatchCuriosityWikipedia  %<>% as.factor() %>% relevel(ref = "No") 


table(bdd$UsesPersoDecideWikipedia)
bdd$UsesPersoDecideWikipedia <- factor(bdd$UsesPersoDecideWikipedia)
bdd$UsesPersoDecideWikipedia %<>% recode_factor(
  "DK-NA-RA" = "No",
  "No, you prefer other offline resource(s) for that" = "No",
  "No, you prefer other online resource(s) for that" = "No"
)
bdd$UsesPersoDecideWikipedia %<>% as.factor() %>% relevel(ref = "No") 


table(bdd$UsesPersoInfoonPoliticsWikipedia)
bdd$UsesPersoInfoonPoliticsWikipedia <- factor(bdd$UsesPersoInfoonPoliticsWikipedia)
bdd$UsesPersoInfoonPoliticsWikipedia %<>% recode_factor(
  "DK-NA-RA" = "No",
  "No, you prefer other offline resource(s) for that" = "No",
  "No, you prefer other online resource(s) for that" = "No"
)
bdd$UsesPersoInfoonPoliticsWikipedia %<>% as.factor() %>% relevel(ref = "No") 

# on how Wikipedia is used in professional activities
## copy without citing source
table(bdd$UsesProCopyingWithoutSourcing)
### sometimes et often grouped together
bdd$UsesProCopyingWithoutSourcing <- factor(bdd$UsesProCopyingWithoutSourcing)
bdd$UsesProCopyingWithoutSourcing %<>% recode_factor(
  "Often" = "Sometimes, at least",
  "Sometimes" = "Sometimes, at least"
)
bdd$UsesProCopyingWithoutSourcing %<>% as.factor() %>% relevel(ref = "Not applicable") 

## copy with citing source
table(bdd$UsesProCopyingWithSourcing)
### sometimes et often grouped together
bdd$UsesProCopyingWithSourcing <- factor(bdd$UsesProCopyingWithSourcing)
bdd$UsesProCopyingWithSourcing %<>% recode_factor(
  "Often" = "Sometimes, at least",
  "Sometimes" = "Sometimes, at least"
)
bdd$UsesProCopyingWithSourcing %<>% as.factor() %>% relevel(ref = "Not applicable") 

## html link to wikipedia article
table(bdd$UsesProRefLinkArticle )
### no need to regroup anything
bdd$UsesProRefLinkArticle  <- factor(bdd$UsesProRefLinkArticle )
bdd$UsesProRefLinkArticle  %<>% as.factor() %>% relevel(ref = "Not applicable") 

## link to source found in wikipedia
table(bdd$UsesProCiteSourceFoundArticle )
### no need to regroup anything
bdd$UsesProCiteSourceFoundArticle  %<>% as.factor() %>% relevel(ref = "Not applicable") 



# Importance of Wikipedia
# as professional source
table(bdd$ImportanceWikipediaProf)
bdd$ImportanceWikipediaProf <- factor(bdd$ImportanceWikipediaProf)
#merge the don't agree and merge those who do not have a clear opinion
bdd$ImportanceWikipediaProf %<>% recode_factor(
  "Somewhat disagree" = "Desagree",
  "Strongly disagree" = "Desagree",
  "You don't know, You have no opinion on the matter" = "No clear opinion",
  "Neutral" = "No clear opinion"
)
bdd$ImportanceWikipediaProf %<>% as.factor() %>% relevel(ref = "No clear opinion") 

#as personal source
table(bdd$ImportanceWikipediaPerso)
bdd$ImportanceWikipediaPerso <- factor(bdd$ImportanceWikipediaPerso)
#merge the don't agree and merge those who do not have a clear opinion
bdd$ImportanceWikipediaPerso %<>% recode_factor(
  "Somewhat disagree" = "Desagree",
  "Strongly disagree" = "Desagree",
  "You don't know, You have no opinion on the matter" = "No clear opinion",
  "Neutral" = "No clear opinion"
)
bdd$ImportanceWikipediaPerso %<>% as.factor() %>% relevel(ref = "No clear opinion") 


# For the culture, to cultivate oneself
table(bdd$ImportanceWikipediaCultivate)
bdd$ImportanceWikipediaCultivate <- factor(bdd$ImportanceWikipediaCultivate)
#merge the don't agree and merge those who do not have a clear opinion
bdd$ImportanceWikipediaCultivate %<>% recode_factor(
  "Somewhat disagree" = "Desagree",
  "Strongly disagree" = "Desagree",
  "You don't know, You have no opinion on the matter" = "No clear opinion",
  "Neutral" = "No clear opinion"
)
bdd$ImportanceWikipediaCultivate %<>% as.factor() %>% relevel(ref = "No clear opinion") 

# globally
table(bdd$ImportanceWikipediaGlobal)
bdd$ImportanceWikipediaGlobal <- factor(bdd$ImportanceWikipediaGlobal)
#merge all those which are not  "A disaster", "Certainly a loss", "Sad, but not that important",  
bdd$ImportanceWikipediaGlobal %<>% recode_factor(
  "Of no impact for you" = "Not that important",
  "Sad, but not that important" = "Not that important", 
  "A No-event" = "Not that important", 
  "A good thing" = "Good-DK-NA-RA",
  "DK-NA-RA" = "Good-DK-NA-RA"
)
bdd$ImportanceWikipediaGlobal %<>% as.factor() %>% relevel(ref = "Not that important") 



# Here preparation of the regressions


## active variables
articlereading <- names(bdd)[str_starts(names(bdd), "PageofAccess")] # how the last article before responding has been "used" some filters apply
usespro <- names(bdd)[str_starts(names(bdd), "UsesPro")] # how the information is accessed to, and the place of wikipedia. For Professional uses
usesperso <- names(bdd)[str_starts(names(bdd), "UsesPerso")] # how the information is accessed to, and the place of wikipedia. For Personal uses



## illustrative variables
discussions <- names(bdd)[str_starts(names(bdd), "DiscussWikipedia")]
perception_externe <- names(bdd)[str_starts(names(bdd), "VisionEnvironment")]
contributions <- c("FirstContribution", "TotalContributionsCount", "ParticipateInArticleDiscussions", "SpecialRoleInProject")
sociodemo <- c("Gender", "Age", "Matrimonial", "Diploma", "ProfessionalSituation", "CSP", "FinancialResourceLevel", "TimeAvailability")
cultural_activities <- c("CulturalActivitiesParticipation", "SportsActivitiesParticipation", "ArtisticActivitiesParticipation", "SocialActivitiesParticipation")
digital_activities <- names(bdd)[str_starts(names(bdd), "DigitalActivities")]
readings <- c("PaperReadingsBooks", "PaperReadingsComics", "PaperReadingsNews", "PaperReadingsFashion", "PaperReadingsSport", "PaperReadingsMusic", 
              "PaperReadingsMovie", "PaperReadingsNature", "PaperReadingsSciences", "PaperReadingsHealth", "PaperReadingsVideoGames", 
              "DigitalReadingsBooks", "DigitalReadingsComics", "DigitalReadingsNews", "DigitalReadingsFashion", "DigitalReadingsSport", "DigitalReadingsMusic", 
              "DigitalReadingsMovie", "DigitalReadingsNature", "DigitalReadingsSciences", "DigitalReadingsHealth", "DigitalReadingsVideoGames")
information <- c("GetInInformationTelevision", "GetInInformationRadio", "GetInInformationNewspaper", "GetInInformationScientificPublication", "GetInInformationSearchEngine", "GetInInformationSocialNetworkSitesInformation",
                 "InformationTrustTelevision", "InformationTrustRadio", "InformationTrustNewspaper", "InformationTrustScientificPublication", "InformationTrustSearchEngine", "InformationTrustSocialNetworkSitesInformation")
digital_skills <- names(bdd)[str_starts(names(bdd), "DigitalSkills")]


groupes <- c(articlereading, usespro, usesperso, discussions, perception_externe, contributions, sociodemo, cultural_activities, digital_activities, readings, information, digital_skills)
taille_groupes <- c(articlereading %>% length(), usespro %>% length(), usesperso %>% length(), discussions %>% length(), perception_externe %>% length(), contributions %>% length(), 
                    sociodemo %>% length(), cultural_activities %>% length(), digital_activities %>% length(), readings %>% length(), information %>% length(), digital_skills %>% length())
bdd <- bdd[, groupes]



## Use of Wikipedia in professional situations regressions
### Settle a discussion in a professional situation
table(bdd$UsesProSettleDebate)
table(bdd$UsesProSettleDebateWikipedia)

bdd$UsesProSettleDebateBin = TRUE
bdd$UsesProSettleDebateBin[bdd$UsesProSettleDebate == "Never" | bdd$UsesProSettleDebate == "Not applicable"] = FALSE
table(bdd$UsesProSettleDebateBin)

bdd$UsesProSettleDebateWikipediaBin = TRUE
bdd$UsesProSettleDebateWikipediaBin[bdd$UsesProSettleDebateWikipedia == "No"] = FALSE
table(bdd$UsesProSettleDebateWikipediaBin)

# ordered variable
bdd$UsesProSettleDebateWikipedia_ordered = 0
bdd$UsesProSettleDebateWikipedia_ordered[bdd$UsesProSettleDebateWikipedia ==  "Yes, but rarely"] = 1 
bdd$UsesProSettleDebateWikipedia_ordered[bdd$UsesProSettleDebateWikipedia == "Yes, sometimes"] = 2
bdd$UsesProSettleDebateWikipedia_ordered[bdd$UsesProSettleDebateWikipedia == "Yes, often"] = 3
table(bdd$UsesProSettleDebateWikipedia_ordered)


name_etape1 <- paste0("UsesProSettleDebateBin"," ~ ", sociodemo[1])
name_etape2 <- paste0("UsesProSettleDebateWikipediaBin"," ~ ", sociodemo[1])
for (j in 2:length(sociodemo)){
  # variables are the same for the time being
  name_etape1 <- paste0(name_etape1," + ", sociodemo[j])
  name_etape2 <- paste0(name_etape2," + ", sociodemo[j])
}

### adding digital skills ####

name_etape1_2 <- paste0(name_etape1," + ", digital_skills[1])
name_etape2_2 <- paste0(name_etape2," + ", digital_skills[1])

for (j in 2:length(digital_skills)){
  # variables are the same for the time being
  name_etape1_2 <- paste0(name_etape1_2," + ", digital_skills[j])
  name_etape2_2 <- paste0(name_etape2_2," + ", digital_skills[j])
}

### adding the environment's point of view regarding the use of Wikipedia: discussio and external perception


name_etape1_3 <- paste0(name_etape1_2," + ", discussions[1])
name_etape2_3 <- paste0(name_etape2_2," + ", discussions[1])

for (j in 2:length(discussions)){
  # variables are the same for the time being
  name_etape1_3 <- paste0(name_etape1_3," + ", discussions[j])
  name_etape2_3 <- paste0(name_etape2_3," + ", discussions[j])
}

for (j in 1:length(perception_externe)){
  # variables are the same for the time being
  name_etape1_3 <- paste0(name_etape1_3," + ", perception_externe[j])
  name_etape2_3 <- paste0(name_etape2_3," + ", perception_externe[j])
}


# no selection variable yet
## socio-demo variables only

Heckman <- heckit(outcome = as.formula(name_etape2), selection = as.formula(name_etape1), data=bdd, method = "2step")
# seems that zero demographic variable explain the use of wikipedia (settle a discussion for professional goal yes)
# not yet 

summary( Heckman)        # summary of the 1st step probit estimation
Heckman$sigma             # estimated sigma
Heckman$rho

# socio-demo plus digital skills

Heckman_2 <- heckit(outcome = as.formula(name_etape2_2), selection = as.formula(name_etape1_2), data=bdd, method = "2step")
# seems that zero demographic variable explain the use of wikipedia (settle a discussion for professional goal yes)
# not yet 

summary( Heckman_2)        # summary of the 1st step probit estimation
Heckman_2$sigma             # estimated sigma
Heckman_2$rho

# socio-demo plus digital skills plus environment

Heckman_3 <- heckit(outcome = as.formula(name_etape2_3), selection = as.formula(name_etape1_3), data=bdd, method = "2step")
# seems that zero demographic variable explain the use of wikipedia (settle a discussion for professional goal yes)
# not yet 

summary( Heckman_3)        # summary of the 1st step probit estimation
Heckman_3$sigma             # estimated sigma
Heckman_3$rho


## for an ordered heckman, probably other tools must be used

#first step: indicate which variables we want for each regression

#change pour chaque régression
formule1 <- liste_var_acp_variables_socio_demo # socio-demo
formule2 <- bind_cols(liste_var_acp_variables_pratiques_culturelles, liste_var_acp_variables_don_engagement_politique) # socio-cultural practicies
formule3 <- liste_var_acp_variables_illustratives_acces_wikipedia # access to Wikipedia
formule4 <- bind_cols(liste_var_acp_variables_contribution_wikipedia,liste_var_acp_variables_licence_wikipedia,liste_var_acp_variables_connaissance_wikipedien,liste_var_acp_variables_preception_wikipedia_profe)
 
name_etape1 <- paste0("G02Q13SQ01_bin ~ ", names(formule1)[1])
ord_name_etape1 <- paste0("G02Q13SQ01_ordered ~ ", names(formule1)[1])
for (j in 2:ncol(formule1)){
  name_etape1 <- paste0(name_etape1," + ", names(formule1)[j])
  ord_name_etape1 <- paste0(ord_name_etape1," + ", names(formule1)[j])
}
name_etape2 <- paste0(name_etape1," + ",names(formule2)[1])
ord_name_etape2 <- paste0(ord_name_etape1," + ",names(formule2)[1])
for (j in 2:ncol(formule2)){
  name_etape2 <- paste0(name_etape2," + ", names(formule2)[j])
  ord_name_etape2 <- paste0(ord_name_etape2," + ", names(formule2)[j])
}
name_etape3 <- paste0(name_etape2," + ",names(formule3)[1])
ord_name_etape3 <- paste0(ord_name_etape2," + ",names(formule3)[1])
for (j in 2:ncol(formule3)){
  name_etape3 <- paste0(name_etape3," + ", names(formule3)[j])
  ord_name_etape3 <- paste0(ord_name_etape3," + ",names(formule3)[j])
}
name_etape4 <- paste0(name_etape3," + ",names(formule4)[1])
ord_name_etape4 <- paste0(ord_name_etape3," + ",names(formule4)[1])  
for (j in 2:ncol(formule4)){
  name_etape4 <- paste0(name_etape4," + ", names(formule4)[j])
  ord_name_etape4 <- paste0(ord_name_etape4," + ", names(formule4)[j])}

reg1 <- glm(name_etape1, data = bdd_propre_discussion_pro, family = binomial(logit))
reg1 <- reg1 %>% tbl_regression(tidy_fun = broom.helpers::tidy_avg_slopes, estimate_fun = scales::label_percent(accuracy = 0.1, style_positive = "plus")) %>% add_significance_stars() %>% modify_column_hide(c("ci", "std.error")) # seulement ce qui est significatif
reg1 <- as_tibble(reg1)
reg2 <- glm(name_etape2, data = bdd_propre_discussion_pro, family = binomial(logit))
reg2 <- reg2 %>% tbl_regression(tidy_fun = broom.helpers::tidy_avg_slopes, estimate_fun = scales::label_percent(accuracy = 0.1, style_positive = "plus")) %>% add_significance_stars() %>% modify_column_hide(c("ci", "std.error")) # seulement ce qui est significatif
reg2 <- as_tibble(reg2)
reg3 <- glm(name_etape3, data = bdd_propre_discussion_pro, family = binomial(logit))
reg3 <- reg3 %>% tbl_regression(tidy_fun = broom.helpers::tidy_avg_slopes, estimate_fun = scales::label_percent(accuracy = 0.1, style_positive = "plus")) %>% add_significance_stars() %>% modify_column_hide(c("ci", "std.error")) # seulement ce qui est significatif
reg3 <- as_tibble(reg3)
reg4 <- glm(name_etape4, data = bdd_propre_discussion_pro, family = binomial(logit))
reg4 <- reg4 %>% tbl_regression(tidy_fun = broom.helpers::tidy_avg_slopes, estimate_fun = scales::label_percent(accuracy = 0.1, style_positive = "plus")) %>% add_significance_stars() %>% modify_column_hide(c("ci", "std.error")) # seulement ce qui est significatif
reg4 <- as_tibble(reg4)

ord_reg1 <- glm(ord_name_etape1, data = bdd_propre_discussion_pro)
ord_reg1 <- ord_reg1 %>% tbl_regression(tidy_fun = broom.helpers::tidy_avg_slopes, estimate_fun = scales::label_percent(accuracy = 0.1, style_positive = "plus")) %>% add_significance_stars() %>% modify_column_hide(c("ci", "std.error")) # seulement ce qui est significatif
ord_reg1 <- as_tibble(ord_reg1)
ord_reg2 <- glm(ord_name_etape2, data = bdd_propre_discussion_pro)
ord_reg2 <- ord_reg2 %>% tbl_regression(tidy_fun = broom.helpers::tidy_avg_slopes, estimate_fun = scales::label_percent(accuracy = 0.1, style_positive = "plus")) %>% add_significance_stars() %>% modify_column_hide(c("ci", "std.error")) # seulement ce qui est significatif
ord_reg2 <- as_tibble(ord_reg2)
ord_reg3 <- glm(ord_name_etape3, data = bdd_propre_discussion_pro)
ord_reg3 <- ord_reg3 %>% tbl_regression(tidy_fun = broom.helpers::tidy_avg_slopes, estimate_fun = scales::label_percent(accuracy = 0.1, style_positive = "plus")) %>% add_significance_stars() %>% modify_column_hide(c("ci", "std.error")) # seulement ce qui est significatif
ord_reg3 <- as_tibble(ord_reg3)
ord_reg4 <- glm(ord_name_etape4, data = bdd_propre_discussion_pro)
ord_reg4 <- ord_reg4 %>% tbl_regression(tidy_fun = broom.helpers::tidy_avg_slopes, estimate_fun = scales::label_percent(accuracy = 0.1, style_positive = "plus")) %>% add_significance_stars() %>% modify_column_hide(c("ci", "std.error")) # seulement ce qui est significatif
ord_reg4 <- as_tibble(ord_reg4)

cat(paste0("## Regression on the explained variable: ",intit_ok,"\n"))
cat("### Binomial regression")
cat("Coefficients\n")
stargazer(reg1,reg2,reg3,reg4,title="Models: CSP (1); (2): (1) + cultural and political practicies; (3): (2) + type of access to Wikipedia; (4): (3) + the knowledge of how wikipedia works",no.space=TRUE,type = "html")
cat("Variance Analysis\n")
print(xtable(anova(reg4)),type = "html")
 cat("Visualisation graphique\n")
# p_(ggcoef_model(reg4, exponentiate = TRUE))#, type = "markdown")
 cat('\n')
 cat("\n\\pagebreak")
 
cat("### Ordered regression")
cat("Coefficients\n")
stargazer(ord_reg1,ord_reg2,ord_reg3,ord_reg4,title="Models: CSP (1); (2): (1) + cultural and political practicies; (3): (2) + type of access to Wikipedia; (4): (3) + the knowledge of how wikipedia works",no.space=TRUE,type = "html")
cat("Variance Analysis\n")
print(xtable(anova(ord_reg4)),type = "html")
 cat("Visualisation graphique\n")
# p_(ggcoef_model(reg4, exponentiate = TRUE))#, type = "markdown")
 cat('\n')
 cat("\n\\pagebreak")

